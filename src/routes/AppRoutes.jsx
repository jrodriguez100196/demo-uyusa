import { BrowserRouter, Route, Routes } from "react-router-dom";
import { HomePage } from "../features/home/pages/HomePage";
import { LoginPage } from "../features/login/pages/LoginPage";
import { Productos } from "../features/productos/pages/Productos";
import { Proveedores } from "../features/proveedores/pages/Proveedores";
import { Clientes } from "../features/clientes/pages/Clientes";
import { Transportistas } from "../features/transportistas/pages/Transportistas";
import { Operarios } from "../features/operarios/pages/Operarios";
import { LibroDeReclamos } from "../features/libroDeReclamos/LibroDeReclamos";
import { Claim } from "../features/claim/pages/Claim";
import { Dashboard } from "../features/dashboard/Dashboard";
import { RegisterOrdenVenta } from "../features/registerOrdenVenta/pages/RegisterOrdenVenta";
import { RegisterPicking } from "../features/registerPicking/pages/RegisterPicking";
import { Checking } from "../features/checking/pages/Checking";
import { RegisterPacking } from "../features/registroPacking/pages/RegisterPacking";
import { Despacho } from "../features/despacho/pages/Despacho";
import {
  OpenSessions,
  ClosedSessions,
  OtherSessions,
  CreateSession,
} from "../features/sessions/pages";

export function AppRoutes() {
  const routerAdmin = [
    {
      path: "/sesiones-abiertas",
      Element: OpenSessions,
    },
    {
      path: "/sesiones-cerradas",
      Element: ClosedSessions,
    },
    {
      path: "/otras-sesiones",
      Element: OtherSessions,
    },
    {
      path: "/crear/sesion",
      Element: CreateSession,
    },
    {
      path: "/productos",
      Element: Productos,
    },
    {
      path: "/proveedores",

      Element: Proveedores,
    },
    {
      path: "/clientes",
      Element: Clientes,
    },
    {
      path: "/transportistas",
      Element: Transportistas,
    },
    {
      path: "/operarios",
      Element: Operarios,
    },
    {
      path: "/reclamos",
      Element: Claim,
    },
    {
      path: "/dashboard",
      Element: Dashboard,
    },
    {
      path: "/ordenventa",
      Element: RegisterOrdenVenta,

    },
    {
      path: "/picking",
      Element: RegisterPicking,

    },{
      path: "/checking",
      Element: Checking,

    },{
      path: "/packing",
      Element: RegisterPacking,

    },{
      path: "/despacho",
      Element: Despacho,

    },

  ];
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/home" element={<Dashboard />} />

        {routerAdmin.map((item, key) => {
          const { Element, path } = item;
          return <Route key={key} path={path} element={<Element />} />;
        })}
      </Routes>
    </BrowserRouter>
  );
}
