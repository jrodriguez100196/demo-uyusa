import React from "react";
import { Page, Text, View, Document, StyleSheet } from "@react-pdf/renderer";

// Create styles
const styles = StyleSheet.create({
  page: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding:"0 70px",
    gap:"20px"
  },
});

// Create Document Component
export const ReactPDF = () => (
  <Document>
    <Page size="A4" style={styles.page}>
      <View style={{fontSize:"12px"}}>
        <Text>Cajabamba, 11 de marzo de 2023</Text>
      </View>
      <View style={{fontSize:"12px"}}>
        <Text>{`Señor (a)`}</Text>
        <Text>XXXXXXXXXXXXXXX</Text>
        <Text>XXXXXXX@XXXXXXXX.com</Text>
        <Text>Cajabamba.-</Text>
        <Text>Presente.-</Text>
      </View>
      <View style={{display:"flex",flexDirection:"column",gap:"15px",fontSize:"12px"}}>
        <View>
          <Text>{`Asunto: Respuesta a Reclamo N° xxxxx-2023, registrado el xx de xxxxxxxx de 2023 registrado en el Libro de Reclamaciones Virtual de nuestra Cooperativa de Ahorro y Crédito Nuestra Señora del Rosario Ltda (COOPAC NSR).`}</Text>
        </View>
        <View>
          <Text>De nuestra consideración:</Text>
        </View>
        <View>
          <Text>{`Por medio de la presente comunicación, nos dirigimos a usted a fin de saludarle muy cordialmente y dar respuesta al reclamo detallado en el asunto. Respecto al referido reclamo, cumplimos con indicarle lo siguiente:`}</Text>
        </View>
        <View>
          <Text>{`Hemos cumplido con verificar que la COOPAC NSR ha actuado de conformidad con la legislación vigente, razón por la cual su reclamo deviene en improcedente.`}</Text>
        </View>
        <View>
          <Text>{`Sin perjuicio de ello, la COOPAC NSR podrá realizar acciones posteriores a fin de verificar si existen mayores elementos que permita reconsiderar la decisión.`}</Text>
        </View>
        <View>
          <Text>Sin otro particular,</Text>
          <Text>Atentamente,</Text>
        </View>
      </View>

      <View style={{fontSize:"12px"}}>
        <Text>{`(firma)`}</Text>
      </View>
      <View style={{fontSize:"12px"}}>
        <Text>XXXXXXXXXX</Text>
      </View>
      <View style={{fontSize:"12px"}}>
        <Text>{`(cargo)`}</Text>
      </View>
    </Page>
  </Document>
);
