import { useRef, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import "primeicons/primeicons.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.css";

import { DataTable } from "primereact/datatable";
import { InputText } from "primereact/inputtext";
import { Column } from "primereact/column";
import { Button } from "primereact/button";

import { Dialog } from "primereact/dialog";

import { MainModal } from "../MainModal/MainModal";
import { useModal } from "@/hooks/useModal";
import { InputLabel, TextField } from "@mui/material";
import style from "./MainDataTable.module.css";
import { Toast } from "primereact/toast";
import { PDFViewer } from "@react-pdf/renderer";
import { ReactPDF } from "./ReactPDF/ReactPDF";
export const MainDataTable = ({
  columns,
  data,
  isFilter,
  isActionUpdate,
  isActionDelete,
  isActionVerify,
  onUpdate,
  onDelete,
}) => {
  const toast = useRef(null);
  const dispatch = useDispatch();
  const [dataTable, setDataTable] = useState(data);
  const [pdfViewModal, setPdfViewModal] = useState(false);
  const [globalFilter, setGlobalFilter] = useState("");
  const { modalStatus, onVisibleModal, onHideModal } = useModal();

  const [idRow, setIdRow] = useState(null);
  const [transConfirm, setTransConfirm] = useState("");

  useEffect(() => {
    setDataTable(data);
  }, [data]);

  const handleGlobalFilter = (e) => {
    setGlobalFilter(e.target.value);
    let dataFiltered = data.filter((item) =>
      item.name.includes(e.target.value)
    );
    setDataTable(dataFiltered);
  };

  const renderHeader2 = () => {
    return (
      <div className="flex justify-content-end">
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            type="search"
            value={globalFilter}
            onChange={handleGlobalFilter}
            placeholder="Ingrese una fecha"
          />
        </span>
      </div>
    );
  };
  const header2 = renderHeader2();

  //---- Seleccionando los datos de una fila

  const onOpenModal = (row) => {
    let id = row.trans_id;
    setIdRow(id);
    onVisibleModal();
  };

  const onCloseModal = () => {
    onHideModal();
    setIdRow(null);
    setTransConfirm("");
  };

  const onSuccessTransaction = () => {
    dispatch(adminUpdateTransactionConfirm(transConfirm, idRow));
    // dispatch(adminApproveTransaction(idRow));
    onCloseModal();
  };
  const onDeclineTransaction = (row) => {
    let id = row.trans_id;
    dispatch(adminDeclineTransaction(id));
  };
  const handleDelete = (row) => {
    let id;
    if (row.id) {
      id = row.id;
    }
    //----Esto es porque en el app esta dando hiscuenta_id en vez de id solo, sera cambiarlo
    if (row.hiscuenta_id) {
      id = row.hiscuenta_id;
    }
    onDelete(id);
  };
  const handleUpdate = (row) => {
    onUpdate(row);
  };

  const buttonSuccess = (rowData) => {
    return (
      <Button
        className="p-button-success p-button-rounded"
        style={{ width: "40px", height: "40px" }}
        type="button"
        icon="pi pi-check"
        // onClick={() => onSuccessTransaction(rowData)}
        onClick={() => {
          toast.current.show({
            severity: "success",
            summary: "Reclamo atendido",
            detail: "El reclamo fue atendido exitosamente",
          });
        }}
      ></Button>
    );
  };
  const buttonDecline = (rowData) => {
    return (
      <Button
        className="p-button-danger p-button-rounded"
        style={{ width: "40px", height: "40px" }}
        type="button"
        icon="pi pi-ban"
        onClick={() => {
          toast.current.show({
            severity: "warn",
            summary: "Reclamo rechazado",
            detail: "El reclamo ha sido rechazado por falta de información",
          });
        }}
      ></Button>
    );
  };
  const buttonPDF = (rowData) => {
    return (
      <Button
        className="p-button-info p-button-rounded"
        style={{ width: "40px", height: "40px" }}
        type="button"
        icon="pi pi-envelope"
        onClick={() => {
          // toast.current.show({
          //   severity: "warn",
          //   summary: "Reclamo rechazado",
          //   detail: "El reclamo ha sido rechazado por falta de información",
          // });
          setPdfViewModal(true);
        }}
      ></Button>
    );
  };

  const footer = (
    <div>
      <Button
        label="Enviar"
        icon="pi pi-check"
        onClick={() => {
          setPdfViewModal(false);

          toast.current.show({
            severity: "success",
            summary: "Enviado al correo",
            detail: "El documento fue enviado al correo del cliente.",
          });
        }}
      />
      <Button
        label="Cancelar"
        icon="pi pi-times"
        onClick={() => setPdfViewModal(false)}
      />
    </div>
  );

  return (
    <>
      <Toast ref={toast} />
      <DataTable
        value={dataTable}
        paginator
        // className='p-datatable-customers'
        rows={7}
        dataKey="id"
        responsiveLayout="scroll"
        header={isFilter ? header2 : null}
        emptyMessage="No se han encontrado resultados."
      >
        {columns &&
          columns.map((column) => {
            if (column.nombre != "Estado") {
              return (
                <Column
                  key={`${column.campo}`}
                  field={column.campo}
                  header={column.nombre}
                  className={column.className}
                  style={{
                    minWidth: "10rem",
                  }}
                />
              );
            }
            if (column.nombre === "Estado") {
              console.log(column);
              return (
                <Column
                  key={`${column.campo}`}
                  field={() => {
                    return (
                      <div className={column.className}> Pendiente</div>
                    );
                  }}
                 
                  header={column.nombre}
                  style={{
                    minWidth: "10rem",
                  }}
                />
              );
            }
          })}
        {/* Botones para verificar transacciones */}
        {isActionVerify && (
          <Column style={{ width: "5rem" }} body={buttonPDF} />
        )}
        {isActionVerify && (
          <Column style={{ width: "5rem" }} body={buttonSuccess} />
        )}
        {isActionVerify && (
          <Column style={{ width: "5rem" }} body={buttonDecline} />
        )}
      </DataTable>

      {/* //---- Modal solo para la transaccion, despues es ideal separarlo de aca */}
      {modalStatus && (
        <MainModal
          header="Confirmar transacción"
          modalStatus={modalStatus}
          onHideModal={onCloseModal}
        >
          <div
            style={{ display: "flex", flexDirection: "column", gap: "10px" }}
          >
            <InputLabel>Transacción para confirmar</InputLabel>

            <TextField
              value={transConfirm || ""}
              size="small"
              fullWidth
              onChange={(e) => setTransConfirm(e.target.value)}
              autoComplete="off"
            />
          </div>

          <button
            className={style.button}
            onClick={() => onSuccessTransaction()}
          >
            GUARDAR
          </button>
        </MainModal>
      )}
      <Dialog
        header="Header Text"
        footer={footer}
        visible={pdfViewModal}
        style={{ width: "50vw" }}
        modal
        onHide={() => setPdfViewModal(false)}
      >
        <PDFViewer style={{ width: "100%", height: "70vh" }}>
          <ReactPDF />
        </PDFViewer>
      </Dialog>
    </>
  );
};
