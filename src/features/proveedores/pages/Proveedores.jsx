import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";
import { ConfigProviderTable } from "../components/ConfigProviderTable/ConfigProviderTable";
import { DataProveedores } from "./assets/DataProveedores";
export const Proveedores = () => {
  const [datas, setDatas] = useState(DataProveedores);
  const columns = [
    { nombre: "Código de Proveedor", campo: "codigo" },
    { nombre: "Nombre (s)", campo: "nombre" },
    { nombre: "Apellido Paterno", campo: "apellido_p" },
    { nombre: "Apellido Materno", campo: "apellido_m" },
    { nombre: "NIF", campo: "nif" },
    { nombre: "Dirección", campo: "direccion" },
    { nombre: "Código Postal", campo: "cp" },
    { nombre: "Distrito", campo: "distrito" },
    { nombre: "Provincia", campo: "provincia" },
    { nombre: "País", campo: "pais" },
    { nombre: "Correo Eléctronico", campo: "email" },
    { nombre: "Teléfono", campo: "telefono" },
  ];
  return (
    <AppStructure>
      <MainHeader title="Proveedores" />

      <ConfigProviderTable columns={columns} data={datas} />
    </AppStructure>
  );
};
