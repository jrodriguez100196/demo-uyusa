import { Button, InputLabel, TextField } from "@mui/material";
import { useRef } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
// import { createNewUserEditor } from "../../../store/slices/admin/thunks";
import style from "./ConfigAddNewProvider.module.css";

import { Toast } from "primereact/toast";
export const ConfigAddNewProvider = ({ onHideModal, handleAdd }) => {
  const dispatch = useDispatch();
  const toast = useRef(null);
  const [newUserEditor, setNewUserEditor] = useState({
    prov_codigo: "",
    prov_nombre: "",
    prov_apellido_p: "",
    prov_apellido_m: "",
    prov_nif: "",
    prov_direccion: "",
    prov_cp: "",
    prov_distrito: "",
    prov_provincia: "",
    prov_pais: "",
    prov_email: "",
    prov_telefono: "",
  });

  const handleCreateNewProvider = () => {
    toast.current.show({
      severity: "success",
      summary: "Proveedor Registrado",
      detail: "El proveedor ha sido agregado exitosamente",
    });
    // return
  };

  return (
    <div className={style.column__container}>
      <Toast ref={toast} />
      <div className={style.column__item}>
        <InputLabel>Código de Provedor</InputLabel>

        <TextField
          value={newUserEditor.prov_codigo}
          name="prov_codigo"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Nombre (s) del Proveedor</InputLabel>

        <TextField
          value={newUserEditor.prd_nombre}
          name="prd_nombre"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Apellido Paterno</InputLabel>

        <TextField
          value={newUserEditor.prov_apellido_p}
          name="prov_apellido_p"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Apellido Materno</InputLabel>

        <TextField
          value={newUserEditor.prov_apellido_m}
          name="prov_apellido_m"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>NIF</InputLabel>

        <TextField
          type="text"
          value={newUserEditor.prov_nif}
          name="prov_nif"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Dirección</InputLabel>

        <TextField
          value={newUserEditor.prov_direccion}
          name="prov_direccion"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Código Postal</InputLabel>

        <TextField
          value={newUserEditor.prov_direccion}
          type="number"
          name="prov_direccion"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Distrito</InputLabel>

        <TextField
          type="text"
          value={newUserEditor.prov_distrito}
          name="prov_distrito"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Provincia</InputLabel>

        <TextField
          type="text"
          value={newUserEditor.prov_provincia}
          name="prov_provincia"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>País</InputLabel>

        <TextField
          type="text"
          value={newUserEditor.prov_pais}
          name="prov_pais"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Email</InputLabel>

        <TextField
          value={newUserEditor.prov_email}
          type="email"
          name="prov_email"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Teléfono</InputLabel>

        <TextField
          value={newUserEditor.prov_telefono}
          name="prov_telefono"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div>
        <Button variant="contained" onClick={handleCreateNewProvider}>
          AGREGAR PROVEEDOR
        </Button>
      </div>
    </div>
  );
};
