import { Button } from "primereact/button";
import { useNavigate } from "react-router-dom";

import logo from "@/assets/logo-reclamos.png";
import style from "./LoginPage.module.css";

import { getUser } from "../../../store/slices/auth/thunks";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

export const LoginPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { login } = useSelector((state) => state.auth);

  useEffect(() => {
    if (login) {
      navigate("/home");
    }
  }, [login]);

  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  const handleLogin = () => {
    // 		email: "hulacio@sempiterno-group.com",
    // 		password: "123456",

    dispatch(getUser(user));
    console.log(user);
  };

  return (
    <>
      <section className={style.login__section}>
        <div className={style.login__container}>
          <div style={{ textAlign: "center", marginBottom: "20px" }}>
            <img src={logo} alt="logo de uwiener" />
          </div>
          <p className={style.login__title}>WMS</p>

          <p className={style.login__title}>Inicia sesión</p>

          <div style={{ width: "100%" }}>
            <form onSubmit={(e) => e.preventDefault()}>
              <div className={style.login__form__item}>
                <label></label>
                <input
                  className={style.login__form__input}
                  type="text"
                  name="email"
                  value={user.email}
                  placeholder="Ingrese nombre o email"
                  onChange={(e) =>
                    setUser((prev) => ({
                      ...prev,
                      [e.target.name]: e.target.value,
                    }))
                  }
                />
              </div>
              <div className={style.login__form__item}>
                <label></label>
                <input
                  className={style.login__form__input}
                  type="password"
                  name="password"
                  value={user.password}
                  placeholder="Ingrese contraseña"
                  onChange={(e) =>
                    setUser((prev) => ({
                      ...prev,
                      [e.target.name]: e.target.value,
                    }))
                  }
                />
              </div>
              <div className={style.login_forgot_pass}>
                <label></label>
                <a href="/forgot-password">Olvidó su contraseña</a>
              </div>

              <Button
                className="p-button-info"
                label="Iniciar sesión"
                style={{ width: "100%" }}
                onClick={() => navigate("/home")}
                // onClick={() => handleLogin()}
              />
            </form>
          </div>
        </div>
      </section>
    </>
  );
};
