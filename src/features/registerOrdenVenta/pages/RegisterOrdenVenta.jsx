import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";

import { ConfigUserTable } from "../components/ConfigUserTable/ConfigUserTable";
import { DataOrdenVenta } from "./assets/DataOrdenVenta";
//importamos el css
import style from "./RegisterOrdenVenta.module.css";

export const RegisterOrdenVenta = () => {
  const [datas, setDatas] = useState(DataOrdenVenta);
  const columns = [
    { nombre: "Código de Producto", campo: "codigo" },
    { nombre: "Descripción", campo: "descripcion" },
    { nombre: "Cantidad", campo: "cantidad" },
    { nombre: "Precio Lista", campo: "preciolista" },
    { nombre: "% de descuento", campo: "descuento" },
    { nombre: "IGV/IGV_EXE", campo: "igv" },
    { nombre: "Total(doc.)", campo: "total" },
    { nombre: "Almacén", campo: "almacen" },
    { nombre: "UM", campo: "um" },
	  { nombre: "Estado", campo: "estado", className: style.status }  ];
  return (
    <AppStructure>
      <MainHeader title="Orden de Venta" />

      <ConfigUserTable columns={columns} data={datas} />
    </AppStructure>
  );
};
