import React from "react";
import style from "./CreateSessionForm.module.css";

export const CreateSessionForm = () => {
	return (
		<div className={style.createSessionForm__container}>
			<div className={style.createSessionForm__header}>
				<h2 className={style.createSessionForm__title}>Crear nueva sesión</h2>
				<hr />
			</div>

			<div className={style.input__container__section}>
				<div className={style.inputGroup__primary}>
					<label>Nombre del evento</label>
					<input type='text' />
				</div>

				<div className={style.inputGroup__general}>
					<div className={style.input__container}>
						<label>Fecha</label>
						<input type='text' />
					</div>
					<div className={style.input__container}>
						<label>Zona horaria</label>
						<input type='text' />
					</div>
					<div className={style.input__container}>
						<label>Plantilla</label>
						<input type='text' />
					</div>
					<div className={style.input__container}>
						<label>Etiquetas</label>
						<input type='text' />
					</div>
					<div className={style.input__container}>
						<label>Organizador</label>
						<input type='text' />
					</div>
					<div className={style.input__container}>
						<label>Responsable</label>
						<input type='text' />
					</div>
					<div className={style.input__container}>
						<label>Sede</label>
						<input type='text' />
					</div>
				</div>

				<button className={style.button}>Crear sesión</button>
			</div>
		</div>
	);
};
