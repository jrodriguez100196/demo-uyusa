import React from "react";
import style from "./SessionCard.module.css";

export const SessionCard = () => {
	return (
		<div className={style.sessionCard__container}>
			<div className={style.sessionCard__info__container}>
				<p className={style.sessionCard__title}>Nombre de la sesión</p>
				<p>
					Lorem ipsum dolor sit amet consectetur, adipisicing elit. Commodi laboriosam nemo
					dignissimos quos provident excepturi reiciendis deserunt accusantium dicta totam quis
					minima vitae velit beatae culpa sunt, obcaecati, explicabo aperiam. Quod autem atque
					ratione reiciendis, non, est aspernatur maxime molestiae laudantium deleniti quo, odit
				</p>
				<p>
					<b>Creador: </b>David Machuca
				</p>
				<p>
					<b>Fecha: </b>12/03/23
				</p>
				<p>
					<b>Hora: </b>20:00 - 22:00
				</p>

				<button className={style.button}>Ver sesión</button>
			</div>
			<div className={style.img__container}>
				<img
					className={style.img}
					src='https://www.foodsovereignty.org/wp-content/uploads/2021/03/OSACTT-informal.png'
					alt='img'
				/>
			</div>
		</div>
	);
};
