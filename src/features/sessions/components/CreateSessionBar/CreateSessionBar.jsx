import React from "react";

import { InputText } from "primereact/inputtext";

import style from "./CreateSessionBar.module.css";
import { Link } from "react-router-dom";

export const CreateSessionBar = () => {
	return (
		<div className={style.createSessionBar__container}>
			<Link to='/crear/sesion' className={style.button}>
				Crear nueva sesión
			</Link>

			<div className='p-input-icon-left'>
				<i className='pi pi-search' />
				<InputText placeholder='Buscar' />
			</div>
		</div>
	);
};
