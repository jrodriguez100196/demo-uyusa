import React from "react";

import { MainHeader } from "@/components/MainHeader/MainHeader";
import { AppStructure } from "@/components/AppStructure/AppStructure.jsx";

import { SessionCard } from "../components/SessionCard/SessionCard";
import { CreateSessionBar } from "../components/CreateSessionBar/CreateSessionBar";

export const OpenSessions = () => {
	return (
		<AppStructure>
			<MainHeader title='Sesiones abiertas RIN' />

			<CreateSessionBar />

			<div
				style={{
					width: "100%",
					display: "flex",
					flexDirection: "column",
					gap: "10px",
					alignItems: "center",
				}}
			>
				<SessionCard />
				<SessionCard />
				<SessionCard />
				<SessionCard />
			</div>
		</AppStructure>
	);
};
