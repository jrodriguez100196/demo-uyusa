import React from "react";

import { MainHeader } from "@/components/MainHeader/MainHeader";
import { AppStructure } from "@/components/AppStructure/AppStructure.jsx";
import { CreateSessionForm } from "../components/CreateSessionForm/CreateSessionForm";

export const CreateSession = () => {
	return (
		<AppStructure>
			<MainHeader title='Sesiones abiertas RIN' />
			<CreateSessionForm />
		</AppStructure>
	);
};
