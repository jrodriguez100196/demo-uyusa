import React, { useState } from "react";
import { Link } from "react-router-dom";
import style from "./HomePage.module.css";
import { FirstStepForm } from "../components/FirstStepForm/FirstStepForm";
import { SecondStepForm } from "../components/SecondStepForm/SecondStepForm";
import { ThirdStepForm } from "../components/ThirdStepForm/ThirdStepForm";
import { QuarterStepForm } from "../components/QuarterStepForm/QuarterStepForm";
import { BackStep } from "../components/BackStep/BackStep";
import { Dialog } from "primereact/dialog";
import { Button } from "primereact/button";

export const HomePage = () => {
  const [step, setStep] = useState(1);
  const [Active, setActive] = useState(false);
  const footer = (
    <div>
      <Button label="Yes" icon="pi pi-check" onClick={() => setActive(false)} />
      <Button label="No" icon="pi pi-times" onClick={() => setActive(false)} />
    </div>
  );
  const onNextStep = () => {
    setStep((prev) => prev + 1);
  };

  const onBackStep = () => {
    if (step > 1) {
      setStep((prev) => prev - 1);
    }
  };

  const onResetStep = () => {
    setStep(1);
    setActive(true);
  };

  return (
    <div className={style.homePage__container}>
      <div className={style.homePage__img__section}></div>
      <div className={style.homePage__content__section}>
        <div className={style.homePage__content__container}>
          {step > 1 && <BackStep onBackStep={onBackStep} />}

          <h2 className={style.homePage__title}>Libro de Reclamaciones</h2>
          {step === 1 && <FirstStepForm onNextStep={onNextStep} />}
          {step === 2 && <SecondStepForm onNextStep={onNextStep} />}
          {step === 3 && <ThirdStepForm onNextStep={onNextStep} />}
          {step === 4 && <QuarterStepForm onResetStep={onResetStep} />}

          <div className={style.additional__info__container}>
            <p className={style.additional__info__text}>
              *La formulación del reclamo no impide acudir a otras vías de
              solución de controversias ni es requisito previo para interponer
              una denuncia ante el INDECOPI.
            </p>
            <p className={style.additional__info__text}>
              * El proveedor debe dar respuesta al reclamo o queja en un plazo
              no mayor a quince (15) días hábiles, el cual es improrrogable.
            </p>
          </div>
        </div>

        <div className={style.footer__container}>
          {`(*) "El [[EMPRESA]] procederá a responder vía telefónica solo en los casos en que el
					reclamo sea declarado procedente de acuerdo a lo establecido en la circular G-184-2015".
					Protección de datos personales: [[EMPRESA]] situado en [[DIRECCIÓN]], en cumplimiento de la Ley de Protección de Datos Personales y su reglamento,
					ha adoptado las medidas de seguridad y confidencialidad necesarias para la protección de
					los datos personales declarados en el presente formulario (en lo sucesivo, los datos); los
					cuales serán tratados para la atención de tu reclamo y almacenados durante el plazo de
					conservación de información aplicable a las empresas del sistema financiero en el banco de
					datos denominado "Reclamos [[EMPRESA]]" con código de registro nro. 01053, el mismo
					que ha sido declarado ante la Autoridad Nacional de Protección de Datos Personales. Los
					datos podrán ser transferidos a terceros domiciliados en territorio nacional y/o
					extranjero –tales como empresas procesadoras, comercios afiliados, entre otros– según la
					naturaleza de tu reclamo y en tanto ello resulte necesario para su absolución, los cuales
					se encuentran listados en [[politica-proteccion-datos]]. Podrás
					ejercer los derechos de acceso, rectificación, cancelación, oposición, revocación e
					información en cualquier momento, presentando ante cualquiera de las oficinas que
					conforman nuestra red una solicitud escrita o los formularios puestos a tu disposición.
					Para más información acerca de nuestra política de privacidad ingresa aquí.`}
        </div>
      </div>
      <div className={style.btnFlotante}>
        <Link to={"/login"}>
          <Button label="Ingresar login" />
        </Link>
      </div>
      <Dialog
        header="Reclamo registrado"
        visible={Active}
        style={{ width: "30vw" }}
        modal
        draggable={false}
        onHide={() => setActive(false)}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            gap: "10px",
          }}
        >
          <i
            className="pi pi-check"
            style={{
              color: "#ffffff",
              fontSize: "70px",
              borderRadius: "50%",
              padding: "30px 30px",
              background: "#46F100",
            }}
          ></i>
          <p style={{ color: "#000000", fontWeight: "600", fontSize: "14px" }}>
            Numero de solicitud:
          </p>
          <p>Nº 0000000001-2023</p>
          <p style={{ color: "#000000", fontWeight: "600", fontSize: "14px" }}>
            Fecha y hora registrada
          </p>
          <p>
            {`${new Date().getDate().toString().padStart(2, "0")}/${(
              new Date().getMonth() + 1
            )
              .toString()
              .padStart(2, "0")}/${new Date().getUTCFullYear()} - ${new Date()
              .getHours()
              .toString()
              .padStart(2, "0")}: ${new Date()
              .getMinutes()
              .toString()
              .padStart(2, "0")}  ${new Date().getHours() >= 12 ? "PM" : "AM"}`}
          </p>
          <p style={{ textAlign: "center", fontSize: "13px" }}>
            <span style={{ color: "red" }}>*</span> Revisar en su bandeja de
            entrada o span de su correo, la información generada del reclamo
          </p>
          <br />
        </div>
      </Dialog>
    </div>
  );
};
