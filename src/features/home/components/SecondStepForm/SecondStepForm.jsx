import React, { useState } from "react";
import { InputText } from "primereact/inputtext";
import { RadioButton } from "primereact/radiobutton";

import { InputTextarea } from "primereact/inputtextarea";

import { Button } from "primereact/button";

import { Dropdown } from "primereact/dropdown";

import style from "./SecondStepForm.module.css";

const tipoReclamo = [{ name: "Reclamo" }, { name: "Queja" }];

export const SecondStepForm = ({ onNextStep }) => {
	const [tipo, setTipo] = useState("");

	return (
		<>
			<div className={style.homePage__check__container}>
				<div className={style.homePage__radio__item}>
					<RadioButton
					// name='pizza'
					// value='Mushroom'
					// onChange={(e) => setIngredient(e.value)}
					// checked={ingredient === "Mushroom"}
					/>
					<label>Producto</label>
				</div>
				<div className={style.homePage__radio__item}>
					<RadioButton
					// name='pizza'
					// value='Mushroom'
					// onChange={(e) => setIngredient(e.value)}
					// checked={ingredient === "Mushroom"}
					/>
					<label>Servicio</label>
				</div>
			</div>

			<div className={style.homePage__form__container}>
				<div className={style.homePage__form__item}>
					<label>Monto reclamado:</label>
					<InputText className='p-inputtext-sm' style={{ width: "300px" }} />
				</div>
				<div className={style.homePage__form__item__area}>
					<label>Descripción:</label>

					<InputTextarea
						// value={value}
						// onChange={(e) => setValue(e.target.value)}
						rows={5}
						cols={30}
						style={{ width: "100%" }}
						autoResize
						placeholder='Coméntanos los detalles de lo sucedido'
					/>
				</div>
			</div>
			<Button label='Continuar' className='p-button-info' onClick={onNextStep} />
		</>
	);
};
