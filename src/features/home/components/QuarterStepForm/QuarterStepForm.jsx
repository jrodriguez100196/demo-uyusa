import React from "react";
import { Button } from "primereact/button";
import { RadioButton } from "primereact/radiobutton";
import { InputTextarea } from "primereact/inputtextarea";

import style from "./QuarterStepForm.module.css";

export const QuarterStepForm = ({ onResetStep }) => {
  return (
    <>
      <div className={style.homePage__form__container}>
        <div className={style.homePage__form__item__area}>
          <label>Observaciones y acciones adoptadas por el proveedor:</label>
          <p style={{ fontSize: "11px", fontWeight: "600" }}>
            * Este campo será llenado por el proveedor al momento de atender su
            queja o reclamo
          </p>
          <InputTextarea
            // value={"Este campo será llenado por el proveedor al momento de atender su queja o reclamo"}
            // onChange={(e) => setValue(e.target.value)}
            rows={3}
            cols={30}
            style={{ width: "100%", resize: "none", overflowY: "auto" }}
            // placeholder='¿Cuál es el pedido del reclamo o queja?'
            disabled={true}
          />
        </div>
        <h2 className={style.title}>¿Dónde quieres recibir tu respuesta?</h2>
        <div className={style.homePage__check__container}>
          <div className={style.homePage__radio__item}>
            <RadioButton
            // name='pizza'
            // value='Mushroom'
            // onChange={(e) => setIngredient(e.value)}
            // checked={ingredient === "Mushroom"}
            />
            <label>Correo electrónico</label>
          </div>
          <div className={style.homePage__radio__item}>
            <RadioButton
            // name='pizza'
            // value='Mushroom'
            // onChange={(e) => setIngredient(e.value)}
            // checked={ingredient === "Mushroom"}
            />
            <label>Domicilio</label>
          </div>
        </div>
      </div>
      <Button label="Enviar" className="p-button-info" onClick={onResetStep} />
    </>
  );
};
