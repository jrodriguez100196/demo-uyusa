import React from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import style from "./FirstStepForm.module.css";

export const FirstStepForm = ({ onNextStep }) => {
	return (
		<>
			<div className={style.homePage__form__container}>
				<div className={style.homePage__form__item}>
					<label>Nombre completo:</label>
					<InputText className='p-inputtext-sm' style={{ width: "300px" }} />
				</div>
				<div className={style.homePage__form__item}>
					<label>DNI / CE:</label>
					<InputText className='p-inputtext-sm' style={{ width: "300px" }} />
				</div>
				<div className={style.homePage__form__item}>
					<label>Domicilio:</label>
					<InputText className='p-inputtext-sm' style={{ width: "300px" }} />
				</div>
				<div className={style.homePage__form__item}>
					<label>Teléfono:</label>
					<InputText className='p-inputtext-sm' style={{ width: "300px" }} />
				</div>
				<div className={style.homePage__form__item}>
					<label>Correo:</label>
					<InputText className='p-inputtext-sm' style={{ width: "300px" }} />
				</div>
				<div className={style.homePage__form__item}>
					<label>Padre o madre:</label>
					<InputText
						className='p-inputtext-sm'
						style={{ width: "300px" }}
						placeholder='En caso de ser menor de edad'
					/>
				</div>
			</div>
			<Button label='Continuar' className='p-button-info' onClick={onNextStep} />
		</>
	);
};
