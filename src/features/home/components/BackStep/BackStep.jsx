import React from "react";
import { IoArrowBackOutline } from "react-icons/io5";
import style from "./BackStep.module.css";

export const BackStep = ({ onBackStep }) => {
	return (
		<div className={style.backStep__container} onClick={onBackStep}>
			<IoArrowBackOutline />
			<p className={style.backStep__text}>Retroceder</p>
		</div>
	);
};
