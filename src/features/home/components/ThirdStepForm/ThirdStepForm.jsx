import React, { useState } from "react";

import { InputTextarea } from "primereact/inputtextarea";
import { Button } from "primereact/button";
import { Dropdown } from "primereact/dropdown";

import style from "./ThirdStepForm.module.css";

const tipoReclamo = [{ name: "Reclamo" }, { name: "Queja" }];

export const ThirdStepForm = ({ onNextStep }) => {
	const [tipo, setTipo] = useState("");

	return (
		<>
			<div className={style.homePage__form__container}>
				<div className={style.homePage__form__item}>
					<label>Tipo de reclamo:</label>
					<Dropdown
						value={tipo}
						onChange={(e) => setTipo(e.value)}
						options={tipoReclamo}
						optionLabel='name'
						placeholder='Selecciona el tipo de reclamo'
						style={{ width: "300px" }}
					/>
				</div>

				<div className={style.claim__info__container}>
					<p className={style.claim__text}>
						<span>*Reclamo: </span>Disconformidad relacionada a los productos o servicios.
					</p>
					<p className={style.claim__text}>
						<span>*Queja: </span>
						Disconformidad no relacionada a los productos o servicios; o, malestar o descontento
						respecto a la atención al público.
					</p>
				</div>

				<div className={style.homePage__form__item__area}>
					<label>Detalle del reclamo o queja:</label>

					<InputTextarea
						// value={value}
						// onChange={(e) => setValue(e.target.value)}
						rows={3}
						cols={30}
						style={{ width: "100%", resize: "none", overflowY: "auto" }}
						placeholder='Coméntanos los detalles de lo sucedido'
					/>
				</div>
				<div className={style.homePage__form__item__area}>
					<label>Pedido del reclamo o queja:</label>
					<InputTextarea
						// value={value}
						// onChange={(e) => setValue(e.target.value)}
						rows={3}
						cols={30}
						style={{ width: "100%", resize: "none", overflowY: "auto" }}
						placeholder='¿Cuál es el pedido del reclamo o queja?'
					/>
				</div>
			</div>
			<Button label='Continuar' className='p-button-info' onClick={onNextStep} />
		</>
	);
};
