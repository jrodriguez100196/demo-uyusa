import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";
import style from "./RegisterPacking.module.css";
import { ConfigUserTable } from "../components/ConfigUserTable/ConfigUserTable";
import { DataPacking } from "./assets/DataPacking";
export const RegisterPacking = () => {
  const [datas, setDatas] = useState(DataPacking);
  const columns = [
    { nombre: "Item", campo: "item" },
    { nombre: "N° Packing", campo: "npacking" },
    { nombre: "Cod Operario", campo: "codoperario" },
    { nombre: "Nombre de Operario", campo: "nameoperario" },
    { nombre: "Fecha Inicio", campo: "finicio" },
    { nombre: "Hora Inicio", campo: "hinicio" },
    { nombre: "Fecha Final", campo: "ffinal" },
    { nombre: "Hora Final", campo: "hfinal" },
    { nombre: "Total Horas", campo: "totalhoras" },
    { nombre: "N° Rotulo", campo: "nrotulo" },
    { nombre: "Total Cajas", campo: "tcajas" },
    { nombre: "Total Peso", campo: "tpeso" },
    { nombre: "Estado", campo: "status", className: style.ready },
  ];
  return (
    <AppStructure>
      <MainHeader title="Packing" />

      <ConfigUserTable columns={columns} data={datas} />
    </AppStructure>
  );
};
