import { Button, InputLabel, TextField } from "@mui/material";
import { useRef } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
// import { createNewUserEditor } from "../../../store/slices/admin/thunks";
import style from "./ConfigAddNewUser.module.css";

import { Toast } from "primereact/toast";
export const ConfigAddNewUser = ({ onHideModal, handleAdd }) => {
  const dispatch = useDispatch();
  const toast = useRef(null);
  const [newUserEditor, setNewUserEditor] = useState({
    prd_nombre: "",
    prd_descripcion: "",
    prd_cantidad: "",
    prd_color: "",
    prd_precio: "",
  });
  // const [newUserEditor, setNewUserEditor] = useState({
  //   usu_nombre: "",
  //   usu_apellido_p: "",
  //   usu_apellido_m: "",
  //   usu_email: "",
  //   usu_password: "",
  // });
 


  const handleCreateNewProduct = () => {
    toast.current.show({
      severity: "success",
      summary: "Producto Registrado",
      detail: "El producto ha sido agregado exitosamente",
    });
    // return
  };

  return (
    <div className={style.column__container}>
      <Toast ref={toast} />
      <div className={style.column__item}>
        <InputLabel>Nombre del Producto</InputLabel>

        <TextField
          value={newUserEditor.prd_nombre}
          name="prd_nombre"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Descripción</InputLabel>

        <TextField
          value={newUserEditor.prd_descripcion}
          name="prd_descripcion"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Cantidad</InputLabel>

        <TextField
        type="number"
          value={newUserEditor.prd_cantidad}
          name="prd_cantidad"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Color</InputLabel>

        <TextField
          value={newUserEditor.prd_color}
          name="prd_color"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div className={style.column__item}>
        <InputLabel>Precio</InputLabel>

        <TextField
          value={newUserEditor.prd_precio}
          type="number"
          name="prd_precio"
          size="small"
          fullWidth
          onChange={(e) =>
            setNewUserEditor((prev) => ({
              ...prev,
              [e.target.name]: e.target.value,
            }))
          }
          autoComplete="off"
        />
      </div>
      <div>
        <Button variant="contained" onClick={handleCreateNewProduct}>
          AGREGAR PACKING
        </Button>
      </div>
    </div>
  );
};
