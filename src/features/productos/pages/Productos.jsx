import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";

import { ConfigUserTable } from "../components/ConfigUserTable/ConfigUserTable";
import {DataUser} from "./assets/DataUser"
export const Productos = () => {
	const [datas,setDatas]=useState(DataUser)
	const columns = [
		{ nombre: "Product ID", campo: "id" },
		{ nombre: "Nombre", campo: "nombre" },
		{ nombre: "Descripción", campo: "descripcion" }, 
		{ nombre: "Color", campo: "color" },
		{ nombre: "Cantidad", campo: "cantidad" },
		{ nombre: "Precio", campo: "precio" },
	];
	return (
		<AppStructure>
			<MainHeader title='Productos' />

			<ConfigUserTable columns={columns} data={datas} />
		</AppStructure>
	);
};
