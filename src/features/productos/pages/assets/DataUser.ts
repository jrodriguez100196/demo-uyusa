export const DataUser = [
  {
    id: 1,
    nombre: "Mesa",
    descripcion: "Madera pulida",
    cantidad: "78",
    color: "Cafe",
    precio: "350",
  },

  {
    id: 2,
    nombre: "Silla",
    descripcion: "Plastico",
    cantidad: "74",
    color : "Azul",
    precio: "35",
  },

  {
    id: 1,
    nombre: "Olla",
    descripcion: "Aluminio",
    cantidad: "4",
    color: "Gris",
    precio: "150",
  },

  {
    id: 1,
    nombre: "Cuchara",
    descripcion: "Plata",
    cantidad: "106",
    color: "Plateado",
    precio: "25",
  },
];
