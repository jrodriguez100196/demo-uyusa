import React from "react";

import { CareChannels } from "../components/CareChannels/CareChannels";
import { MainCover } from "../components/MainCover/MainCover";
import { PreferentialExchange } from "../components/PreferentialExchange/PreferentialExchange";
import { SecureTransactions } from "../components/SecureTransactions/SecureTransactions";
import { CallToAction } from "../components/CallToAction/CallToAction";
import { LandingHeader } from "../../../components/LandingHeader/LandingHeader";

export const LandingPage = () => {
	return (
		<>
			<LandingHeader />
			<MainCover />

			{/* ---- /Nosotros ---- */}

			<PreferentialExchange />

			<SecureTransactions />

			<CallToAction />

			<CareChannels />

			{/* <ImmediateOperations /> */}

			{/* <section className='section__container'>
				<ContactUs />
			</section> */}
		</>
	);
};
