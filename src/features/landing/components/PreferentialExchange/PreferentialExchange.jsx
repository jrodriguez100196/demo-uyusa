import imageIdea from "../../assets/idea.png";

import style from "./PreferentialExchange.module.css";

export const PreferentialExchange = () => {
	return (
		<div className={style.preferentialExchange__container}>
			<div className={style.preferentialExchange__info__container}>
				<div className={style.preferentialExchange__info__subtitle}>
					<hr />
					<h2 className={style.preferentialExchange__info__title}>Nosotros</h2>
				</div>

				<p className={style.preferentialExchange__info__text}>
					La RIN es la primera organización intermediadora de oportunidades empresariales y de
					negocios a nivel internacional. A través de su plataforma de negocios, su red de business
					brokers asociados, así como sus afiliados y agentes en cada uno de los países en donde
					opera, la RIN busca facilitar el contacto y cierre de negocios en diversos aspectos tales
					como:
				</p>

				<div className={style.item__list__container}>
					<div className={style.item__list}>
						1. Compra o venta de empresas (o líneas de negocio).
					</div>
					<div className={style.item__list}>
						2. Búsqueda o colocación de financiamiento para empresa.
					</div>
					<div className={style.item__list}>3. Búsqueda de socios capitalistas o de negocios.</div>
					<div className={style.item__list}>
						4. Intermediación de productos (commodities) en el mercado internacional.
					</div>
				</div>
			</div>

			<img
				src={imageIdea}
				alt='Picture of the author'
				className={style.preferentialExchange__image}
			/>
		</div>
	);
};
