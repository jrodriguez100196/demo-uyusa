import style from "./SecureTransactions.module.css";

export const SecureTransactions = () => {
	return (
		<div className={style.secureTransactions__container}>
			<div className={style.secureTransactions__info__subtitle}>
				<hr />
				<h2 className={style.secureTransactions__info__title}>Aliados Institucionales</h2>
			</div>

			<div className={style.aliados__info}>
				La RIN viene convocando a nivel internacional a las principales cámaras y gremios de cada
				país a fin de participar activamente en la Plataforma de Negocios (en sus condiciones de
				aliados), permitiéndole a dichas organizaciones (así como a sus propios afiliados o
				asociados) emplear esta herramienta de interacción como un espacio de publicidad y contacto
				entre las ofertas y demandas de oportunidades empresariales facilitando de esta manera el
				desarrollo de los negocios a nivel internacional.
			</div>
			<div className={style.aliados__logos__container}>
				<a href='http://cocep.org.pe/'>
					<img
						src='https://red-in.com/wp-content/uploads/sites/2/2018/07/CAMARA-ESPA•OLA-2-240x71.png'
						className='center-block'
					/>
				</a>
				<a href='http://www.cciperu.it/'>
					<img
						src='https://red-in.com/wp-content/uploads/sites/2/2020/01/LogoCCIP-completo-1967-actualizado-240x97.png'
						className='center-block'
					/>
				</a>
				<a href='http://www.ccpm.org.pe'>
					<img
						src='https://red-in.com/wp-content/uploads/sites/2/2018/07/CAMARA-MEXICANA-2-240x71.png'
						className='center-block'
					/>
				</a>
				<a href='http://www.capechi.org.pe/'>
					<img
						src='https://red-in.com/wp-content/uploads/sites/2/2018/07/CAPECHI-2-240x71.png'
						className='center-block'
					/>
				</a>
				<a href='http://www.amcham.org.pe/'>
					<img
						src='https://red-in.com/wp-content/uploads/sites/2/2018/07/AMCHAM-2-153x100.png'
						className='center-block'
					/>
				</a>
				<a href='https://www.camaralima.org.pe/'>
					<img
						src='https://red-in.com/wp-content/uploads/sites/2/2018/07/CCL-2-240x71.png'
						className='center-block'
					/>
				</a>
				<a href='http://www.camaraperuchile.org/'>
					<img
						src='https://red-in.com/wp-content/uploads/sites/2/2018/07/CCPh-2-240x71.png'
						className='center-block'
					/>
				</a>
				<a href='http://www.clubempresarial.pe/'>
					<img
						src='https://red-in.com/wp-content/uploads/sites/2/2018/07/CLUB-EMPRESARIAL-2.png'
						className='center-block'
					/>
				</a>
			</div>
		</div>
	);
};
