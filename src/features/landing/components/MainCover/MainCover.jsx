// import { Calculator } from "@molecules/Calculator/Calculator";
import style from "./MainCover.module.css";

import imgCover from "../../assets/social-networking.png";
import { Link } from "react-router-dom";

export const MainCover = () => {
	return (
		<div className={style.mainCover}>
			<div className={style.mainCover__ilustracion__container}>
				<img src={imgCover} alt='ilustracion' />
			</div>
			<div className={style.mainCover__info}>
				<div className={style.mainCover__info__text__container}>
					<p className={style.mainCover__info__title}>Encuentra oportunidades empresariales</p>
					<p
						className={`${style.mainCover__info__title} ${style.mainCover__info__title__secondary}`}
					>
						Compra y venta de empresas, búsqueda o colocación de financiamiento, búsqueda de socios
					</p>
				</div>

				<p className={style.mainCover__info__text}>
					Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aperiam sed quod modi
					repudiandae. Expedita eos placeat enim perferendis, corrupti accusantium fugiat soluta
					odit mollitia neque esse minima nostrum tenetur officiis.
				</p>

				<div className={style.mainCover__cta}>
					<Link to='http://localhost:5173/login' className={style.mainCover__cta__button}>
						Descubre nuevas oportunidades
					</Link>
				</div>
			</div>
		</div>
	);
};
