import contactImg from "../../assets/contact-img.png";

import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import { Button } from "primereact/button";

import style from "./CareChannels.module.css";

export const CareChannels = () => {
	return (
		<div className={style.careChannels__container}>
			<img src={contactImg} alt='imagen de dubai' className={style.careChannels__image} />

			<div className={style.careChannels__info__container}>
				<div className={style.careChannels__info__subtitle}>
					<hr />
					<h2 className={style.careChannels__info__title}>Contáctanos</h2>
				</div>

				<div className={style.rowForm__container}>
					<div className={style.rowForm}>
						<label>Tu Nombre:</label>
						<InputText />
					</div>
					<div className={style.rowForm}>
						<label>Tu Correo:</label>
						<InputText />
					</div>
					<div className={style.rowForm}>
						<label>Tu Mensaje:</label>
						<InputTextarea autoResize rows={5} cols={30} />
					</div>
					<Button label='Enviar mensaje' className='p-button-info' />
				</div>
			</div>
		</div>
	);
};
