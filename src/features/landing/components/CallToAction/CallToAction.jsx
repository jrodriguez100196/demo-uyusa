import { Link } from "react-router-dom";
import style from "./CallToAction.module.css";

export const CallToAction = () => {
	return (
		<div className={style.callToAction__container}>
			<div className={style.callToAction__text}>Encuentra oportunidades de negocios</div>
			<div className={style.mainCover__cta}>
				<Link to='http://localhost:5173/login' className={style.mainCover__cta__button}>
					Ingresar a nuestra plataforma
				</Link>
			</div>
		</div>
	);
};
