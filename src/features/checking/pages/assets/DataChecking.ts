export const DataChecking = [
  {
    codigo: "LNV4100",
    descripcion: "Martillo Deluxe M/Madera 29MM",
    ubicacion: "14002",
    caja:"10000",
    ploking: "24.00",
    capacidad: "NIU",
    box: "6",
  },
  {
    codigo: "BIS5841",
    descripcion: "Disco Lija con Velcro Soun/Set 115MM #100 UYU",
    ubicacion: "16G0784",
    caja:"0.1667",
    ploking: "4.00",
    capacidad: "SET",
    box: "2",
  },
  
    {
      codigo: "HUG1245",
      descripcion: "Broca mix 16PCS-MM (PIE/MAC/CONC) UYU",
      ubicacion: "11T0414",
      caja:"0.0833",
      ploking: "25.00",
      capacidad: "SET",
      box: "5",
    },
  
];
