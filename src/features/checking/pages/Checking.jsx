import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";
import style from "./Checking.module.css";
import { ConfigUserTable } from "../components/ConfigUserTable/ConfigUserTable";
import {DataChecking} from "./assets/DataChecking"
export const Checking = () => {
	const [datas,setDatas]=useState(DataChecking)
	const columns = [
		{ nombre: "Codigo", campo: "codigo" },
		{ nombre: "Descripción", campo: "descripcion" }, 
		{ nombre: "Ubicación", campo: "ubicacion" },
		{ nombre: "Caja", campo: "caja" },
		{ nombre: "Ploking Liberado", campo: "ploking" },
		{ nombre: "Capacidad", campo: "capacidad" },
		{ nombre: "Box", campo: "box" },
		{ nombre: "Estado", campo: "estado", className: style.status },

	];
	return (
		<AppStructure>
			<MainHeader title='Checking' />

			<ConfigUserTable columns={columns} data={datas} />
		</AppStructure>
	);
};
