import React, { useState } from "react";
import { Chart } from "primereact/chart";
import { MainHeader } from "../../components/MainHeader/MainHeader";
import { AppStructure } from "../../components/AppStructure/AppStructure";
import { SectionStructure } from "../../components/SectionStructure/SectionStructure";
import style from "./Dashboard.module.css";
import PaneldeNotificaciones from "../../components/PaneldeNotificaciones/PaneldeNotificaciones";
import { color } from "@mui/system";
import {
  chartDataDashboard,
  lightOptionsDashboard,
  basicDataDashboard,
  getLightTheme,
  Panelnoti,
} from "./assets/DataDashboard";
export function Dashboard() {
  const [chartData] = useState(chartDataDashboard);

  const [lightOptions] = useState(lightOptionsDashboard);

  const [basicData] = useState(basicDataDashboard);

  const { basicOptions } = getLightTheme();
  return (
    <AppStructure>
      <MainHeader title="Dashboard" actionButton={true} />
      <SectionStructure>
        <div
          style={{
            display: "flex",
            gap: "20px",
            justifyContent: "space-around",
            flexWrap: "wrap",
            fontWeight: "600",
          }}
        >
          {Panelnoti?.map((item, key) => {
            return (
              <div
                style={{
                  width: "300px",
                  height: "auto",
                  background: "#232C3D",
                  display: "flex",
                  flexDirection: "column",
                  gap: "10px",
                  padding: "20px 0px",
                  borderRadius: "15px",
                  color: "#fff",
                  margin: "0 0 30px",
                }}
              >
                <span
                  style={{
                    width: "100%",
                    textAlign: "center",
                    fontSize: "20px",
                  }}
                >
                  {item.name}
                </span>
                <div style={{ width: "100%", textAlign: "center" }}>
                  {" "}
                  {item.cantidad}
                </div>
                <div style={{ width: "100%", textAlign: "center" }}>
                  <i
                    className={`pi ${item.icon}`}
                    style={{ fontSize: "30px" }}
                  />
                </div>
              </div>
            );
          })}
        </div>
        <h1 style={{ width: "100%", textAlign: "center" }}>
          Gráficos estadísticos
        </h1>
        <div className={style.diagram__container}>
          <div className={`${style.centerDiagramaAnillo}`}>
            <Chart
              type="doughnut"
              data={chartData}
              options={lightOptions}
              style={{ position: "relative", width: "40%" }}
            />
          </div>

          <div>
            <h5>Diagrama mensual</h5>
            <Chart type="bar" data={basicData} options={basicOptions} />
          </div>
        </div>
      </SectionStructure>
    </AppStructure>
  );
}
