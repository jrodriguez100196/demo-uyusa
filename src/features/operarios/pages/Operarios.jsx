import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";
import { ConfigProviderTable } from "../components/ConfigProviderTable/ConfigProviderTable";
import { DataOperarios } from "./assets/DataOperarios";
export const Operarios = () => {
  const [datas, setDatas] = useState(DataOperarios);
  const columns = [
    { nombre: "Cod. Operario", campo: "codoperario" },
    { nombre: "Nombre", campo: "nombre" },
    { nombre: "Apellidos", campo: "apellidos" },
    
  ];
  return (
    <AppStructure>
      <MainHeader title="Operarios" />

      <ConfigProviderTable columns={columns} data={datas} />
    </AppStructure>
  );
};
