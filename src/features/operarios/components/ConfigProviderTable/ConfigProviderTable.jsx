import { useModal } from "@/hooks/useModal";
import { Button } from "primereact/button";
import { Toolbar } from "primereact/toolbar";
import style from "./ConfigProviderTable.module.css";
import { SectionStructure } from "../../../../components/SectionStructure/SectionStructure";
import { MainDataTable } from "../../../../primeComponents/MainDataTable/MainDataTable";
import { MainModal } from "../../../../primeComponents/MainModal/MainModal";
import { ConfigAddNewProvider } from "../ConfigAddNewProvider/ConfigAddNewProvider";
// import { exportToExcel } from "../../../util";
export const ConfigProviderTable = ({ columns, data, isFilter }) => {
  const { modalStatus, onVisibleModal, onHideModal } = useModal();

  return (
    <>
      <SectionStructure>
        <Toolbar
          className="mb-4"
          left={
            <div className={style.flex}>
              <Button
                label="AGREGAR OPERARIO"
                icon="pi pi-plus"
                className="p-button-sm p-button-info mr-2"
                size="normal"
                onClick={onVisibleModal}
              />
              {/* <Button
								label='DESCARGAR'
								icon='pi pi-file-excel'
								className='p-button-sm p-button-info mr-2'
								onClick={() => {
									exportToExcel(data, "Lista_de_Usuarios");
								}}
							/> */}
            </div>
          }
        />

        <MainDataTable columns={columns} data={data} isFilter={isFilter} />
      </SectionStructure>

      <MainModal
        modalStatus={modalStatus}
        onHideModal={onHideModal}
        header="AGREGAR OPERARIO"
      >
        <ConfigAddNewProvider  onHideModal={onHideModal}/>
      </MainModal>
    </>
  );
};
