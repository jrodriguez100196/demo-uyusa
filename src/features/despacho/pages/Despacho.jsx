import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";
import style from "./Despacho.module.css";
import { ConfigUserTable } from "../components/ConfigUserTable/ConfigUserTable";
import {DataDespacho} from "./assets/DataDespacho"
export const Despacho = () => {
	const [datas,setDatas]=useState(DataDespacho)
	const columns = [
		/*{ nombre: "Ruta", campo: "ruta" },
		{ nombre: "N° Cotización", campo: "ncotizacion" },
		{ nombre: "N° Guía", campo: "nguia" }, 
		{ nombre: "Clientes", campo: "clientes" },
		{ nombre: "Forma Envío", campo: "fenvio" },
		{ nombre: "Distrito", campo: "distrito" },
		{ nombre: "Zona", campo: "zona" },
		{ nombre: "Rotulo", campo: "rotulo" },
		{ nombre: "Cajas", campo: "cajas" },
		{ nombre: "Kg", campo: "kg" },
		{ nombre: "M3", campo: "m3" },
		{ nombre: "Estado", campo: "status", className: style.status },*/
		{ nombre: "Ta Number", campo: "ta_number" },
		{ nombre: "Placa del Vehículo", campo: "vehicule_number" },
		{ nombre: "Ciudad", campo: "ciudad" },
		{ nombre: "Supervisor", campo: "supervisor" },
		{ nombre: "Fecha", campo: "fecha" },
		{ nombre: "Estado", campo: "estado", className: style.status},

	];
	return (
		<AppStructure>
			<MainHeader title='Despacho' />

			<ConfigUserTable columns={columns} data={datas} />
		</AppStructure>
	);
};
