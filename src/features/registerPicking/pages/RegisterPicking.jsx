import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";
import style from "./RegisterPicking.module.css";
import { ConfigUserTable } from "../components/ConfigUserTable/ConfigUserTable";
import { DataPicking } from "./assets/DataPicking";
export const RegisterPicking = () => {
  const [datas, setDatas] = useState(DataPicking);
  const columns = [

    { nombre: "Ruta", campo: "ruta" },
		{ nombre: "N° Cotización", campo: "ncotizacion" },
		{ nombre: "N° Guía", campo: "nguia" }, 
		{ nombre: "Clientes", campo: "clientes" },
		{ nombre: "Forma Envío", campo: "fenvio" },
		{ nombre: "Distrito", campo: "distrito" },
		{ nombre: "Zona", campo: "zona" },
		{ nombre: "Rotulo", campo: "rotulo" },
		{ nombre: "Cajas", campo: "cajas" },
		{ nombre: "Kg", campo: "kg" },
		{ nombre: "M3", campo: "m3" },
		{ nombre: "Estado", campo: "status", className: style.status }
  ];
  return (
    <AppStructure>
      <MainHeader title="Picking" />

      <ConfigUserTable columns={columns} data={datas} />
    </AppStructure>
  );
};
