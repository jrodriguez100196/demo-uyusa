export const DataPicking = [

  {
    ruta: 1,
    ncotizacion: "220025035",
    nguia: "75883",
    clientes: "Reyes Mendoza Miriam",
    fenvio: "DIRECCION",
    Distrito: "LIMA",
    zona: "CC. Malvinas 3 Int. 4 Pta 4",
    rotulo: "DIRECCION",
    cajas: "1",
    kg: "5.12",
    m3: "0.01",
  },
  {
    ruta: 1,
    ncotizacion: "245025035",
    nguia: "85754",
    clientes: "SANTA CRUZ GLOBAL INVERSIONES S.A.C.",
    fenvio: "DIRECCION",
    Distrito: "LIMA",
    zona: "CC. Malvinas 3 Int. 84 Pta 1",
    rotulo: "DIRECCION",
    cajas: "1",
    kg: "33",
    m3: "0.09",
  },
];
