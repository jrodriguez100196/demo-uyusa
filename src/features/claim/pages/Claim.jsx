import React, { useState } from "react";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { MainHeader } from "@/components/MainHeader/MainHeader";

import { ConfigUserTable } from "../components/ConfigUserTable/ConfigUserTable";
import {
  ClaimAutomatico,
  ClaimAtendido,
  ClaimDesatendido,
  ClaimGeneral,
} from "../assets/DataClain";
export const Claim = () => {
  const [datas, setDatas] = useState(ClaimGeneral);
  const columns = [
    { nombre: "ID", campo: "id" },
    { nombre: "Reclamo", campo: "reclamo" },
    { nombre: "Estado", campo: "estado" },
    { nombre: "Fecha", campo: "fecha" },
  ];
  const Desatendido = () => {
    setDatas(ClaimDesatendido);
  };
  const Atendido = () => {
    setDatas(ClaimAtendido);
  };
  const Automatico = () => {
    setDatas(ClaimAutomatico);
  };
  const General = () => {
    setDatas(ClaimGeneral);
  };
  return (
    <AppStructure>
      <MainHeader title="Gestionar reclamos" />

      <ConfigUserTable
        columns={columns}
        data={datas}
        Desatendido={Desatendido}
        Atendido={Atendido}
        Automatico={Automatico}
        General={General}
      />
    </AppStructure>
  );
};
