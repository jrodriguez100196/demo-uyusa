export const ClaimGeneral = [
  {
    id: 1,
    reclamo: "RE00000001",
    estado: "Desatendido",
    fecha: "23/02/2023",
  },

  {
    id: 2,
    reclamo: "RE00000002",
    estado: "Atendido",
    fecha: "12/02/2023",
  },

  {
    id: 3,
    reclamo: "RE00000003",
    estado: "Automático",
    fecha: "10/02/2023",
  },

  {
    id: 4,
    reclamo: "RE00000004",
    estado: "Automático",
    fecha: "10/02/2023",
  },

  {
    id: 5,
    reclamo: "RE00000005",
    estado: "Automático",
    fecha: "10/02/2023",
  },

  {
    id: 6,
    reclamo: "RE00000006",
    estado: "Atendido",
    fecha: "13/02/2023",
  },

  {
    id: 7,
    reclamo: "RE00000007",
    estado: "Atendido",
    fecha: "15/02/2023",
  },

  {
    id: 8,
    reclamo: "RE00000006",
    estado: "Desatendido",
    fecha: "01/03/2023",
  },
];

export const ClaimDesatendido = [
  {
    id: 1,
    reclamo: "RE00000001",
    estado: "Desatendido",
    fecha: "23/02/2023",
  },
  {
    id: 8,
    reclamo: "RE00000006",
    estado: "Desatendido",
    fecha: "01/03/2023",
  },
];

export const ClaimAtendido = [
  {
    id: 2,
    reclamo: "RE00000002",
    estado: "Atendido",
    fecha: "12/02/2023",
  },
  {
    id: 6,
    reclamo: "RE00000006",
    estado: "Atendido",
    fecha: "13/02/2023",
  },
  {
    id: 7,
    reclamo: "RE00000007",
    estado: "Atendido",
    fecha: "15/02/2023",
  },
];

export const ClaimAutomatico = [
  {
    id: 3,
    reclamo: "RE00000003",
    estado: "Automático",
    fecha: "10/02/2023",
  },

  {
    id: 4,
    reclamo: "RE00000004",
    estado: "Automático",
    fecha: "10/02/2023",
  },

  {
    id: 5,
    reclamo: "RE00000005",
    estado: "Automático",
    fecha: "10/02/2023",
  },
];
