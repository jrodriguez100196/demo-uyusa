import { useModal } from "@/hooks/useModal";

import { Button } from "primereact/button";
import { Toolbar } from "primereact/toolbar";
import { SectionStructure } from "../../../../components/SectionStructure/SectionStructure";
import { MainDataTable } from "../../../../primeComponents/MainDataTable/MainDataTable";
import { MainModal } from "../../../../primeComponents/MainModal/MainModal";
import style from "./ConfigUserTable.module.css";

// import { exportToExcel } from "../../../util";

export const ConfigUserTable = ({
  columns,
  data,
  isFilter,
  Desatendido,
  Atendido,
  Automatico,
  General,
}) => {
  const { modalStatus, onVisibleModal, onHideModal } = useModal();

  return (
    <>
      <SectionStructure>
        <Toolbar
          className="mb-4"
          left={
            <div className={style.flex}>
              <Button
                label="TODOS LOS DESATENDIDO"
                className="p-button-sm p-button-info mr-2"
                size="normal"
                onClick={Desatendido}
              />
              <Button
                label="TODOS LOS ATENDIDO"
                className="p-button-sm p-button-info mr-2"
                size="normal"
                onClick={Atendido}
              />
              <Button
                label="TODOS LOS AUTOMÁTICO"
                className="p-button-sm p-button-info mr-2"
                size="normal"
                onClick={Automatico}
              />
              <Button
                label="TODOS LOS RECLAMOS"
                className="p-button-sm p-button-info mr-2"
                size="normal"
                onClick={General}
              />
            </div>
          }
        />
        <MainDataTable
          columns={columns}
          data={data}
          isFilter={isFilter}
          isActionUpdate={true}
          isActionDelete={true}
          isActionVerify={true}
        />
      </SectionStructure>

      <MainModal
        modalStatus={modalStatus}
        onHideModal={onHideModal}
        header="Crear usuario"
      >
        {/* <ConfigAddNewUser /> */}
      </MainModal>
    </>
  );
};
