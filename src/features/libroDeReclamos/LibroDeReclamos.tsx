import React, { useRef, useState } from "react";

import style from "./LibroDeReclamos.module.css";
import InputTextForm from "../../components/InputTextForm/InputTextForm";
import { Button } from "primereact/button";
import { RadioButton } from "primereact/radiobutton";
import { Checkbox } from "primereact/checkbox";
import { Link } from "react-router-dom";
import { Toast } from "primereact/toast";
export function LibroDeReclamos() {
  const toast = useRef<Toast>(null);
  const [data, setData] = useState({
    Agencia:"",
    nombre:"",
    Apellidos:"",
    Tipo_de_Document:"",
    Numero_de_Documento:"",
    Departamento:"",
    Provincia:"",
    Distrito:"",
    Dirección:"",
    Referencia:"",
    Telefono:"",
    Celular:"",
    Correo_Electronico:"",
    Padre:"",
    Via_del_Reclamo:"",
    Medio_de_Respuesta:"",
    Codigo_de_Operacion:"",
    Motivo:"",
    Numero_de_la_Cuenta:"",
    Numero_de_Tarjeta:"",
    Moneda:"",
    Monto_Reclamado:"",
    Tipo_de_Seguro:""
  });
  const [view, setview] = useState(true);
  const [selectSINO, setSelectSINO] = useState("Si");
  const [selectSINODos, setSelectSINODos] = useState("No");
  const [checked, setChecked] = useState(false);
  const [selectDetalleReclamo, setselectDetalleReclamo] = useState("Reclamo");
  const [selectIdentificacionBien, setselectIdentificacionBien] =
    useState("Producto");
  const onInputChange = (e: any) => {
    setData((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };
  const HandleSubmit = () => {
    if (toast.current) {
      toast.current.show({
        severity: "success",
        summary: "Reclamo creado",
        detail: "El reclamo fue creado exitosamente",
      });
    }
    setview(false);
  };
  return (
    <div className={style.container}>
      <Toast ref={toast} />
      {view ? (
        <div className={style.container_card}>
          <div className={style.container_card_general}>
            <div>
              <h1 style={{ width: "100%", textAlign: "center" }}>
                Libro de reclamación
              </h1>
              <br />
              <h3 style={{ width: "100%", textAlign: "center" }}>
                ¿Es cliente de la empres?
              </h3>
              <br />
              <div style={{ display: "flex", justifyContent: "center" }}>
                <div style={{ display: "flex", gap: "20px" }}>
                  {["Si", "No"].map((item, key) => {
                    return (
                      <div key={key} className="field-radiobutton">
                        <RadioButton
                          inputId={item}
                          name="category"
                          value={item}
                          onChange={(e) => setSelectSINO(e.value)}
                          checked={selectSINO === item}
                        />{" "}
                        <label htmlFor={item}>{item}</label>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
            <br />
            <div>
              <InputTextForm
                nameLabel={"Agencia u Oficina donde se Originó el Reclamo:"}
                nameInput={"Agencia"}
                dataInput={data.Agencia}
                onInputChange={onInputChange}
                disabled={false}
              />
              <div className={style.flexwidth}>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Apellidos:"}
                    nameInput={"Apellidos"}
                    dataInput={data.Apellidos}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Nombre:"}
                    nameInput={"nombre"}
                    dataInput={data.nombre}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
              </div>
              <div className={style.flexwidth}>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Tipo de Documento:"}
                    nameInput={"Tipo_de_Document"}
                    dataInput={data.Tipo_de_Document}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Número de Documento:"}
                    nameInput={"Numero_de_Documento"}
                    dataInput={data.Numero_de_Documento}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
              </div>
              <div className={style.flexwidth}>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Departamento:"}
                    nameInput={"Departamento"}
                    dataInput={data.Departamento}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Provincia:"}
                    nameInput={"Provincia"}
                    dataInput={data.Provincia}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Distrito:"}
                    nameInput={"Distrito"}
                    dataInput={data.Distrito}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
              </div>
              <div className={style.flexwidth}>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Dirección"}
                    nameInput={"Dirección"}
                    dataInput={data.Dirección}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Referencia:"}
                    nameInput={"Referencia"}
                    dataInput={data.Referencia}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
              </div>
              <div className={style.flexwidth}>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Teléfono"}
                    nameInput={"Telefono"}
                    dataInput={data.Telefono}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Celular:"}
                    nameInput={"Celular"}
                    dataInput={data.Celular}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Correo Electrónico:"}
                    nameInput={"Correo_Electronico"}
                    dataInput={data.Correo_Electronico}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
              </div>

              <InputTextForm
                nameLabel={"Padre o Madre (Para caso de Menor de Edad):"}
                nameInput={"Padre"}
                dataInput={data.Padre}
                onInputChange={onInputChange}
                disabled={false}
              />
              <InputTextForm
                nameLabel={"Via del Reclamo:"}
                nameInput={"Via_del_Reclamo"}
                dataInput={data.Via_del_Reclamo}
                onInputChange={onInputChange}
                disabled={false}
              />
              <br />
              <div className={style.flexwidth}>
                <div style={{ minWidth: "200px" }}>
                  <label htmlFor={"DetalleDeLaReclamación"}>
                    Detalle de la Reclamación
                  </label>
                  <br />
                  <br />
                  <div style={{ display: "flex", gap: "10px" }}>
                    {["Reclamo", "Queja"].map((item, key) => {
                      return (
                        <div key={key} className="field-radiobutton">
                          <RadioButton
                            inputId={item}
                            name="category"
                            value={item}
                            onChange={(e) => setselectDetalleReclamo(e.value)}
                            checked={selectDetalleReclamo === item}
                          />{" "}
                          <label htmlFor={item}>{item}</label>
                        </div>
                      );
                    })}
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                  }}
                >
                  <div>
                    <p>
                      <b style={{ color: "red" }}>RECLAMO:</b> Disconformidad
                      relacionada a los productos o servicios.
                    </p>
                    <br />
                    <p>
                      <b style={{ color: "red" }}>QUEJA:</b> Disconformidad no
                      relacionada a los productos o servicios: o, malestar o
                      descontento respecto a la atención al público.
                    </p>
                  </div>
                </div>
              </div>
              <br />
              <div className={style.flexwidth}>
                <div>
                  <label htmlFor={"DetalleDeLaReclamación"}>
                    Identificación del bien
                  </label>
                  <br />
                  <br />
                  <div style={{ display: "flex", gap: "10px" }}>
                    {["Producto", "Servicio"].map((item, key) => {
                      return (
                        <div key={key} className="field-radiobutton">
                          <RadioButton
                            inputId={item}
                            name="category"
                            value={item}
                            onChange={(e) =>
                              setselectIdentificacionBien(e.value)
                            }
                            checked={selectIdentificacionBien === item}
                          />{" "}
                          <label htmlFor={item}>{item}</label>
                        </div>
                      );
                    })}
                  </div>
                  <br />
                </div>
              </div>
              <InputTextForm
                nameLabel={"Medio de Respuesta:"}
                nameInput={"Medio_de_Respuesta"}
                dataInput={data.Medio_de_Respuesta}
                onInputChange={onInputChange}
                disabled={false}
              />
              <InputTextForm
                nameLabel={"Código de Operación:"}
                nameInput={"Codigo_de_Operacion"}
                dataInput={data.Codigo_de_Operacion}
                onInputChange={onInputChange}
                disabled={false}
              />
              <InputTextForm
                nameLabel={"Motivo:"}
                nameInput={"Motivo"}
                dataInput={data.Motivo}
                onInputChange={onInputChange}
                disabled={false}
              />
              <div className={style.flexwidth}>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Número de la Cuenta:"}
                    nameInput={"Numero_de_la_Cuenta"}
                    dataInput={data.Numero_de_la_Cuenta}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Número de Tarjeta:"}
                    nameInput={"Numero_de_Tarjeta"}
                    dataInput={data.Numero_de_Tarjeta}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
              </div>
              <div className={style.flexwidth}>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Moneda:"}
                    nameInput={"Moneda"}
                    dataInput={data.Moneda}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Monto Reclamado:"}
                    nameInput={"Monto_Reclamado"}
                    dataInput={data.Monto_Reclamado}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
                <div className={style.flexwidthDiv}>
                  <InputTextForm
                    nameLabel={"Tipo de Seguro:"}
                    nameInput={"Tipo_de_Seguro"}
                    dataInput={data.Tipo_de_Seguro}
                    onInputChange={onInputChange}
                    disabled={false}
                  />
                </div>
              </div>
              {/* <div>Input file</div> */}
              <br />
              <div>
                <p>Detalle del Reclamo:</p>
                <textarea
                  name=""
                  id=""
                  cols={30}
                  rows={10}
                  style={{ width: "100%", maxWidth: "100%", minWidth: "100%" }}
                ></textarea>
                <br />
                <br />
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "right",
                  }}
                >
                  <div
                    style={{
                      width: "500px",
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <p>Contador de Caracteres(Máximo 1500)</p>

                    {/* <Button label={"Contador"} /> */}
                  </div>
                </div>
              </div>
              <br />
              <div>
                <p>
                  <span>¿Usted viene en representación de un usuario?</span>
                </p>
                <br />
                <div style={{ display: "flex", gap: "20px" }}>
                  {["Si", "No"].map((item, key) => {
                    return (
                      <div key={key} className="field-radiobutton">
                        <RadioButton
                          inputId={item}
                          name="categorys"
                          value={selectSINODos}
                          onChange={(e) => setSelectSINODos(e.value)}
                          checked={selectSINODos === item}
                        />{" "}
                        <label htmlFor={item}>{item}</label>
                      </div>
                    );
                  })}
                </div>
              </div>
              <br />
              <div>
                <label htmlFor="">
                  {"Términos y condiciones del uso de datos personales:"}
                </label>
                <br />
                <br />
                <textarea
                  name=""
                  id=""
                  cols={30}
                  rows={10}
                  style={{ width: "100%", maxWidth: "100%", minWidth: "100%" }}
                ></textarea>
              </div>
              <br />
              <br />
              <div
                className="field-checkbox"
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <Checkbox
                  inputId="binary"
                  checked={checked}
                  onChange={(e) => setChecked(e.checked ? e.checked : false)}
                />{" "}
                <label htmlFor="binary">
                  He leído y acepto las condiciones
                </label>
              </div>
              <br />
              <div className={style.Boton}>
                <Button label="Registrar" onClick={HandleSubmit} />
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div
          style={{
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div
            style={{
              background: "#ffffff",
              padding: "20px 30px",
              border: "1px solid #dfe3e9",
              width: "500px",
              height: "500px",
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <i
              className="pi pi-check-circle"
              style={{ fontSize: "10em", margin: "0 auto", color: "green" }}
            ></i>
            <br />
            <br />
            <div style={{ textAlign: "center" }}>
              <h2>Reclamo registrado correctamente</h2>
              <br />
              <p>
                Revise si bandeja de entrada de su correo el reclamo registrado
                actualmente.
              </p>
            </div>
          </div>
        </div>
      )}

      <div className={style.btnFlotante}>
        <Link to={"/login"}>
          <Button label="Ingresar login" />
        </Link>
      </div>
    </div>
  );
}
