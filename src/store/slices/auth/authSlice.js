import { createSlice } from "@reduxjs/toolkit";

export const authSlice = createSlice({
	name: "auth",
	initialState: {
		isLoading: false,
		login: null,
	},
	reducers: {
		isLoading: (state) => {
			state.isLoading = true;
		},
		setLogin: (state, action) => {
			state.login = action.payload;
			state.isLoading = false;
		},
	},
});

export const { isLoading, setLogin } = authSlice.actions;
