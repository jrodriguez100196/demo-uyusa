import { authApi } from "../../../connections";
import { setLogin } from "./authSlice";

export const getUser = (payload) => {
	return async (dispatch) => {
		try {
			const { data } = await authApi.post("/v1/token?grant_type=password", payload, {
				headers: {
					apikey:
						"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InNzbmtyYXBqbXZyZ25vY3hqeGVkIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzcxMDgwMTIsImV4cCI6MTk5MjY4NDAxMn0.Io6N5Dayg8XHjEim_4faWkNKyA5vdsiWYffjjIpTY3U",
				},
			});

			localStorage.setItem("refreshToken", data.refresh_token);

			dispatch(setLogin(data));
		} catch (error) {
			console.log(error);
		}
	};
};

export const useRefreshToken = (payload) => {
	return async (dispatch) => {
		try {
			const { data } = await authApi.post("/v1/token?grant_type=refresh_token", payload, {
				headers: {
					apikey:
						"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InNzbmtyYXBqbXZyZ25vY3hqeGVkIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzcxMDgwMTIsImV4cCI6MTk5MjY4NDAxMn0.Io6N5Dayg8XHjEim_4faWkNKyA5vdsiWYffjjIpTY3U",
				},
			});

			localStorage.setItem("refreshToken", data.refresh_token);

			dispatch(setLogin(data));
			console.log("se refresco el token");
		} catch (error) {
			console.log(error);
		}
	};
};

export const logoutUser = () => {
	return (dispatch) => {
		localStorage.removeItem("refreshToken");
		dispatch(setLogin(null));
	};
};
