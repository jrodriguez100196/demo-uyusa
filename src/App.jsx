import React from "react";

import { AppRoutes } from "./routes/AppRoutes";

import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRefreshToken } from "@/store/slices/auth/thunks";
import { isLoading } from "./store/slices/auth/authSlice";
import { addLocale, locale } from "primereact/api";
export const App = () => {
  addLocale("es", {
    firstDayOfWeek: 1,
    dayNames: [
      "Domingo",
      "Lunes",
      "Martes",
      "Miércoles",
      "Jueves",
      "Viernes",
      "Sábado",
    ],
    dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
    dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
    monthNames: [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ],
    monthNamesShort: [
      "Ene",
      "Feb",
      "Mar",
      "Abr",
      "May",
      "Jun",
      "Jul",
      "Ago",
      "Sep",
      "Oct",
      "Nov",
      "Dic",
    ],
    today: "Hoy",
    clear: "Limpiar",
  });

  locale("es");

  const dispatch = useDispatch();

  useEffect(() => {
    async function cargarUsuario() {
      const refreshToken = localStorage.getItem("refreshToken");
      if (!refreshToken) {
        return;
      }

      try {
        dispatch(isLoading());
        dispatch(useRefreshToken({ refresh_token: refreshToken }));
      } catch (error) {
        console.log(error);
      }
    }

    cargarUsuario();
  }, []);
  return (
    <>
      <AppRoutes />
    </>
  );
};
