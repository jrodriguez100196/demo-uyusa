import React from "react";
import { Button } from "primereact/button";
export function DialogFooterCancelarConfirmar({ onHide, onSubmit }) {
  return (
    <>
      <Button label="Cancelar" icon="pi pi-times" onClick={onHide} />
      <Button label="Confirmar" icon="pi pi-check" onClick={onSubmit} />
    </>
  );
}
