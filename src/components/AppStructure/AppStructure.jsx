import Sidebar from "../Sidebar/Sidebar";

import style from "./AppStructure.module.css";

export const AppStructure = ({ children }) => {
	return (
		<div className={style.appStructure__container}>
			<Sidebar />

			<div className={style.appStructure__mainContent}>{children}</div>
		</div>
	);
};
