import React from "react";
import { Dialog } from "primereact/dialog";

function AdminDialog({
  Header,
  visible,
  onHide,
  footer,
  styleWidth,
  BodyDialog,
}) {
  return (
    <Dialog
      header={Header}
      visible={visible}
      modal
      onHide={onHide}
      footer={footer}
      style={{ width: `${styleWidth}vw` }}
    >
      <BodyDialog />
    </Dialog>
  );
}

export default AdminDialog;
