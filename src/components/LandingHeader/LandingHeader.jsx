import { Link } from "react-router-dom";

import logo from "@/assets/logo-reclamos.png";
import style from "./LandingHeader.module.css";

export const LandingHeader = () => {
	return (
		<div className={style.mainNavBar__container}>
			<div>
				<img className={style.logo} src={logo} alt='logo de rin academy' />
			</div>

			<ul className={style.mainNavBar__list}>
				<li className='li '>
					<Link to='/login' rel='noopener noreferrer'>
						<button className={style.button}>INICIAR SESIÓN</button>
					</Link>
				</li>
			</ul>
		</div>
	);
};
