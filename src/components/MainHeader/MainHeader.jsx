import { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { IoPersonOutline, IoLogOutOutline } from "react-icons/io5";
import PersonIcon from "@mui/icons-material/Person";

import { logoutUser } from "../../store/slices/auth/thunks";
import { Toast } from "primereact/toast";
import logo from "@/assets/logo-reclamos.png";
import style from "./MainHeader.module.css";
import { Button } from "primereact/button";

export const MainHeader = ({ title = "", actionButton = false }) => {
  const toast = useRef(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [menuActive, setMenuActive] = useState(false);

  const handleNavigate = (path) => {
    navigate(`/${path}`);
    setMenuActive(false);
  };

  const handleLogout = () => {
    dispatch(logoutUser());
    navigate("/login");
  };
  const Desatendido = () => {
    toast.current.show({
      severity: "warn",
      summary: "Reclamo desatendido",
      detail: "El reclamo salio de la fecha limite de los 15 días",
    });
  };
  const Atendido = () => {
    toast.current.show({
      severity: "success",
      summary: "Reclamo atendido",
      detail: "El reclamo fue atendido exitosamente",
    });
  };
  const Automático = () => {
    toast.current.show({
      severity: "info",
      summary: "Reclamo atendido automáticamente",
      detail: "El reclamo fue atendido de manera automática",
    });
  };
  return (
    <header className={style.mainHeader__container}>
      <Toast ref={toast} />
      <div style={{ display: "flex", gap: "10px", alignItems: "center" }}>
        <p className={style.mainHeader__title}>{title}</p>
        {actionButton && (
          <>
            <Button
              label={"Alerta Desatendido"}
              className="p-button-warning"
              onClick={Desatendido}
            />
            <Button
              label={"Alerta Atendido"}
              className="p-button-success"
              onClick={Atendido}
            />
            <Button
              label={"Alerta Automático"}
              className="p-button-info"
              onClick={Automático}
            />
          </>
        )}
      </div>
      <div className={style.mainHeader__navbar__container}>
        <div style={{ position: "relative" }}>
          <div
            className={style.mainHeader__profile}
            onClick={() => setMenuActive((prev) => !prev)}
          >
            <PersonIcon />
          </div>
          {menuActive && (
            <div className={style.profileMenu}>
              <ul className={style.profileMenu__list}>
                <li
                  className={style.profileMenu__item}
                  onClick={() => handleNavigate("home")}
                >
                  <IoPersonOutline size={20} /> Mis Datos
                </li>
                <li className={style.profileMenu__item} onClick={handleLogout}>
                  <IoLogOutOutline size={20} /> Cerrar Sesión
                </li>
              </ul>
            </div>
          )}
        </div>
      </div>
    </header>
  );
};
