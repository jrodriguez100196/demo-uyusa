import { Drawer, List, Stack, Toolbar } from "@mui/material";

import SidebarItem from "./SidebarItem";
import SidebarItemCollapse from "./SidebarItemCollapse";
import AlignVerticalBottomIcon from "@mui/icons-material/AlignVerticalBottom";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import SwitchAccountIcon from "@mui/icons-material/SwitchAccount";
import LocalShipping  from "@mui/icons-material/LocalShipping";
import Inventory from "@mui/icons-material/Inventory";
import AnnouncementIcon from "@mui/icons-material/Announcement";
import SettingsIcon from '@mui/icons-material/Settings';
import BookIcon from '@mui/icons-material/Book';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import DvrIcon from '@mui/icons-material/Dvr';
import logo from "@/assets/logo-reclamos.png";

const Sidebar = () => {
  const appRoutes = [
    {
      path: "/Home",
      index: true,
      state: "home",
    },
    {
      path: "/dashboard",
      index: true,
      state: "home",
      sidebarProps: {
        displayText: "Dashboard",
        icon: <AlignVerticalBottomIcon />,
      },
    },
    {
      path: "#",
      sidebarProps: {
        displayText: "Archivos Maestro",
        icon: <ManageAccountsIcon />,
      },
      child: [
        {
          path: "/productos",
          sidebarProps: {
            displayText: "Registro de Productos",
          },
        },
        {
          path: "/clientes",
          sidebarProps: {
            displayText: "Registro de Clientes",
          },
        },
        {
          path: "/proveedores",
          sidebarProps: {
            displayText: "Registro de Proveedores",
          },
        },
        {
          path: "/transportistas",
          sidebarProps: {
            displayText: "Registro de Transportistas",
          },
        },
        {
          path: "/operarios",
          sidebarProps: {
            displayText: "Registro de Operarios",
          },
        },
      ],
    }, {
      path: "#",
      sidebarProps: {
        displayText: "Procesos de Almacén",
        icon: <Inventory />,
      },
      child: [
        {
          path: "/ordenventa",
          sidebarProps: {
            displayText: "Registro Orden de Venta",
          },
        },
        {
          path: "/picking",
          sidebarProps: {
            displayText: "Registro de Picking",
          },
        },
        {
          path: "/checking",
          sidebarProps: {
            displayText: "Checking",
          },
        },
        {
          path: "/packing",
          sidebarProps: {
            displayText: "Registro de Packing",
          },
        }, 
        {
          path: "/despacho",
          sidebarProps: {
            displayText: "Despacho",
          },
        },
      ],
    },
    {
      path: "#",
      sidebarProps: {
        displayText: "Definición de Rutas",
        icon: <LocalShipping />,
      },
      child: [
        {
          path: "/reclamos",
          sidebarProps: {
            displayText: "Reclamos",
          },
        },
      ],
    },
    
    
    
    {
      path: "#",
      sidebarProps: {
        displayText: "Configuración",
        icon: <SettingsIcon />,
      },
      child: [
        {
          path: "#",
          sidebarProps: {
            displayText: "Configuración",
          },
        },
      ],
    },
  ];
  return (
    <Drawer
      variant="permanent"
      sx={{
        width: "320px",
        flexShrink: 0,
        "& .MuiDrawer-paper": {
          width: "320px",
          boxSizing: "border-box",
          borderRight: "0px",
          backgroundColor: "#232c3d",
          color: "#fff",
        },
      }}
    >
      <List disablePadding>
        <Toolbar sx={{ marginBottom: "20px" }}>
          <Stack
            sx={{ width: "100%", padding: "20px 0" }}
            direction="row"
            justifyContent="center"
          >
            <img src={logo} alt="logo" style={{ width: "100%" }} />
            {/* <h1 style={{textAlign: 'center', fontSize: '25px'}}>Sistema de Reclamos</h1> */}
          </Stack>
        </Toolbar>

        {appRoutes.map((route, index) =>
          route.sidebarProps ? (
            route.child ? (
              <SidebarItemCollapse item={route} key={index} />
            ) : (
              <SidebarItem item={route} key={index} />
            )
          ) : null
        )}
      </List>
    </Drawer>
  );
};

export default Sidebar;
