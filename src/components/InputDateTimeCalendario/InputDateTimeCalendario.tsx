import { Calendar } from "primereact/calendar";
import React from "react";
import style from "./InputDateTimeCalendario.module.css";
export default function InputDateTimeCalendario({
  nameLabel,
  nameInput,
  dataInput,
  onInputChange,
  disabled,
}) {
  let name =
    nameLabel.charAt(0).toUpperCase() + nameLabel.slice(1).toLowerCase();

  return (
    <>
      <div className="field">
        <label htmlFor={nameLabel.toLowerCase()}>{name}</label>
        <br />
        <Calendar
          dateFormat="dd/mm/yy"
          id={nameInput}
          locale={"es"}
          name={nameInput}
          value={dataInput}
          onChange={(e) => onInputChange(e, nameInput, true)}
          showIcon
          disabled={disabled}
          className={style.Calendar}
        />
      </div>
    </>
  );
}
