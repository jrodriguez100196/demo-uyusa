import React from "react";
import { InputText } from "primereact/inputtext";
import style from "./InputTextForm.module.css";
export default function InputTextForm({
  nameLabel,
  nameInput,
  dataInput,
  onInputChange,
  disabled,
}) {
  let name =
    nameLabel.charAt(0).toUpperCase() + nameLabel.slice(1).toLowerCase();

  return (
    <>
      <div className="field">
        <label htmlFor={nameLabel.toLowerCase()}>{name.toUpperCase()}</label>
        <br />
        <InputText
          id={nameInput}
          value={dataInput}
          onChange={onInputChange}
          autoComplete="off"
          disabled={disabled}
          className={`${style.InputText}`}
        />
      </div>
    </>
  );
}
