import React from "react";
import { Button } from "primereact/button";
function DialogFooterCancelarGuardar({ onHide, onSubmit }) {
  return (
    <>
      <Button label="Cancelar" icon="pi pi-times" onClick={onHide} />
      <Button label="Guardar" icon="pi pi-check" onClick={onSubmit} />
    </>
  );
}

export default DialogFooterCancelarGuardar;